<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php');
	include 'postman/_variables.php';
	
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = '';
	$layout = 'subpage';
?>

<?php
	include("header.php");
?>


<div class="row">
	<div class="small-12 columns">
		<div class="main-content">
			<div class="row">
				<div class="small-12 columns">
					<h2><?=$aOutput['title']; ?></h2>

					<div class="divider"></div>
					<?=$aOutput['msg']; ?>
				</div>
<!--				<div class="medium-4 columns">
				</div> -->
			</div>
		</div>
		
		<div class="divider"></div>

		<?php
			include("connected.php");
		?>
	</div>
</div>

<?php
	include("footer.php");
?>