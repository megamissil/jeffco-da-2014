<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php');
	include 'postman/_variables.php';

	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = '';
	$layout = 'subpage';

	$aUpdates = pageByCategory("UPDATES", "ALL", 0, 20, "PUBL_ASC");
?>

<?php
	include("header.php");
?>


<div class="row">
	<div class="small-12 columns">
		<div class="main-content">
			<div class="row">
				<div class="small-12 columns">
					<h2>Press Releases</h2>

					<div class="divider"></div>
			
					<dl class="accordion" data-accordion="">
					    <dd class="accordion-navigation">     
							<?php foreach( $aUpdates as $aUpdate) { ?>
						    	<a href="#panel-<?php echo $aUpdate['pageid']; ?>"><?php echo $aUpdate['title']; ?></a>
							    <div id="panel-<?php echo $aUpdate['pageid']; ?>" class="content">
									<?php echo $aUpdate['msg']; ?>
							    </div>
							<?php } ?>
					    </dd>
					</dl>
				</div>
			</div>
		</div>
		
		<div class="divider"></div>

		<?php
			include("connected.php");
		?>
	</div>
</div>

<?php
	include("footer.php");
?>