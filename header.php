<!doctype html>
<html class="no-js" lang="en">
  <!-- <![endif] -->
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width" />
    <meta content='<?php echo $cMetaDesc; ?>' name='description'>
    <meta content='<?php echo $cMetaKW; ?>' name='keywords'>
    <title>Jefferson County District Attorney | <?php echo $cPageTitle; ?></title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <link rel="stylesheet" href="stylesheets/zeekee_support.css" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Buenard:400,700' rel='stylesheet' type='text/css'>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-57951616-1', 'auto');
    ga('send', 'pageview');

    </script>
  </head>
  <body class="<?=$layout ?>">

<header>

<div class="contain-to-grid">
    <div class="logo-wrapper">
      <div class="large-offset-8 hide-for-small columns">
        <a href="index.php"><img id="logo" src="img/logo-small.png" alt="sse-logo"></a>
      </div>
      <div class="large-offset-8 show-for-small-only columns" align="center">
        <a href="index.php"><img id="logo" src="img/logo-small.png" alt="sse-logo"></a>
      </div>
    </div>

    <nav class="top-bar" data-topbar>
      <ul class="title-area">
        <li class="name">
          <h1><a href="#"></a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
      </ul>

      <section class="top-bar-section" align="center">
        <ul>
          <li><a href="index.php">Home</a></li>
          <li class="has-dropdown"><a href="#">The D.A.'s Office</a>
            <ul class="dropdown">
              <li><a href="about.php">About the District Attorney</a></li>
              <li><a href="office-location.php">Office Location</a></li>
              <li><a href="employment-opportunities.php">Employment Opportunities</a></li>
              <li><a href="contact-us.php">Contact Us</a></li>
            </ul>
          </li>
          <li class="has-dropdown"><a href="#">Prosecution Division</a>
            <ul class="dropdown">
              <li><a href="criminal-felony-procedure.php">Criminal Felony Procedure</a></li>
              <li><a href="charging.php">Charging</a></li>
              <li><a href="white-collar.php">White Collar Crimes/Public Corruption</a></li>
              <li><a href="grand-jury.php">Grand Jury</a></li>
              <li><a href="district-court.php">District Court</a></li>
              <li><a href="circuit-court.php">Circuit Court</a></li>
              <li><a href="family-court.php">Family Court</a></li>
              <li class="has-dropdown"><a href="#">Special Services Division</a>
                <ul class="dropdown">
                  <li><a href="worthless-check.php">Worthless Check Enforcement Unit</a></li>
                  <li><a href="restitution-recovery.php">Restitution Recovery Unit</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="has-dropdown"><a href="#">Deferred Prosecution Programs</a>
            <ul class="dropdown">
              <li><a href="defensive-driving-school.php">Defensive Driving School</a></li>
              <li><a href="deferred-prosecution-program.php">DUI Deferred Prosecution Program</a></li>
              <li><a href="drug-court.php">Drug Court</a></li>
              <li><a href="veteran-court.php">Veteran’s Court</a></li>
            </ul>
          </li>

          <li class="has-dropdown"><a href="#">Victim Assistance</a>
            <ul class="dropdown">
              <li><a href="victim-services.php">Victim Services</a></li>
              <li><a href="resources.php">Resources</a></li>
              <li><a href="brochures.php">Brochures & Forms</a></li>
              <li><a href="domestic-violence.php">Domestic Violence</a></li>
              <li><a href="prescott-house.php">Prescott House</a></li>
              <li><a href="elder-abuse.php">Elder Abuse</a></li>
              <li><a href="identity-theft.php">Identity Theft</a></li>
              <li><a href="http://alex.state.al.us/stopbullying/" target="_blank">Stop Bullying in Alabama</a></li>
            </ul>
          </li>

          <li class="has-dropdown"><a href="#">Protecting the Community</a>
            <ul class="dropdown">
              <li><a href="newborns.php">Secret Safe Place for Newborns</a></li>
              <li class="has-dropdown"><a href="check-enforcement-unit.php">Check Enforcement Unit</a>
                <ul class="dropdown">
                  <li><a href="most-wanted-check.php">Most Wanted</a></li>
                  <li><a href="merchant-resources.php">Merchant Resources</a></li>
                  <li><a href="defendant-resources.php">Defendant Resources</a></li>
                </ul>
              </li>
              <li class="has-dropdown"><a href="restitution-recovery-unit-information-for-victims.php">Restitution Recovery Unit</a>
                <ul class="dropdown">
                  <li><a href="most-wanted.php">Most Wanted</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="has-dropdown"><a href="#">Resources</a>
            <ul class="dropdown">
              <li><a href="court-directory.php">Court Directory</a></li>
              <li><a href="law-enforcement-directory.php">Jefferson County Law Enforcement Directory</a></li>
              <li><a href="overview-of-the-criminal-justice-system.php">Overview of the Criminal Justice System</a></li>
              <li><a href="http://10jc.alacourt.gov/jury.html" target="_blank">Jury Information</a></li>
              <li><a href="http://sheriff.jccal.org/" target="_blank">Who's In Jail</a></li>
              <li><a href="crime-statistics.php">Crime Statistics</a></li>
              <li><a href="links.php">Links</a></li>
            </ul>
          </li>

          <li class="has-dropdown"><a href="#">Media</a>
              <ul class="dropdown">
                <li><a href="press-releases.php">Press Releases</a></li>
                <li><a href="in-the-media.php">In The Media</a></li>
                <li><a href="press-contact.php">Press Contact</a></li>
              </ul>
          </li>
        </ul>
      </section>
	</div>
</header>
