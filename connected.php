<?php
  include($_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php');
  require_once('postman/_variables.php');

	$aWarrants = pageByCategory("WARRANTS", "ALL", 0, 2, "PUBL_DESC");
?>



<div class="row">
		<div class="medium-4 small-12 columns">
			<div class="social-box col-height">
				<h5>Stay Connected</h5>
				<ul>
					<li><a href="mailto:davier@jccal.org"><img src="/img/email.png" alt="email"></a></li>
					<li><a href="https://www.facebook.com/pages/Jefferson-County-District-Attorney-Birmingham-Division/447703795254249?sk=timeline" target="_blank"><img src="/img/facebook.png" alt="facebook"></a></li>
					<li><a href="contact-us.php"><img src="/img/phone.png" alt="phone"></a></li>
				</ul>
				<div class="divider"></div>
				<p><a href="mailto:davier@jccal.org">Join the DA Email List</a><br /> 
				<a href="https://www.facebook.com/pages/Jefferson-County-District-Attorney-Birmingham-Division/447703795254249?sk=timeline" target="_blank">Like the DA FB page</a><br />
				<a href="#" data-reveal-id="report">Anonymously report</a></p>
					<div id="report" class="reveal-modal report-reveal small" data-reveal>
						<!-- <form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data"> -->
						<form action="/postman/anonymous-report.php" method="POST" id="foonster" name="foonster">
							<div class="report-form" align="center">
								<div class="row">
									<div class="large-12 columns">
										<p class="report-label">Please provide as much information as possible:</p>
										<textarea name="msg" id="msg" cols="25" rows="25"></textarea>
									</div>
								</div>
							</div>
							<input class="button" type="submit" name="mybutton" value="Submit">
						</form>
						<a href="index.php" id="close">&#215;</a>
					</div>
			</div>
		</div>

		<div class="medium-8 small-12 columns">
			<div class="warrant-box col-height">
				<div class="row">
					<div class="medium-4 hide-for-small columns">
						<a href="http://www.crimestoppersmetroal.com/" target="_blank"><img id="stoppers" src="img/crime-stoppers.png" alt="" align="center"></a>
					</div>
					<div class="medium-8 columns">
						<ul class="no-bullet">
							<?php foreach( $aWarrants as $aWarrant) {?>
							<?php $warrantPage = pageGet($aWarrant['pageid']) ?>
							<li>
								<a href="<?=$aWarrant['url'] ?>">
								<img src="<?=$warrantPage['images'][1]['image'] ?>" alt=""></a>
							</li>
							<?php } ?>
						<a href="/most-wanted-check.php"><p>Click here to see who has an outstanding<br /> ARREST WARRANT</p></a>
					</div>
				</div>
			</div>
		</div>
</div>