﻿<?php

	require_once( $_SERVER['DOCUMENT_ROOT'] . '/tyfoon/connect.php' );

	$cPageTitle = 'Error';

	include( '_header.php' );

?>

<h1 class="flabel">Page Not Found</h1>

<h3 class="fcontent">The requested page was not found.</h3>

<?php

	include( '_footer.php' );
	
?>	