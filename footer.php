
<footer>	
	<div class="row">
		<div class="small-12 columns">
			<div class="address-bar">
				<p>Brandon K. Falls <span>&#149;</span> Jefferson County District Attorney <span>&#149;</span> 205-325-5252<br />
				801 Richard Arrington Jr. Blvd. N., Birmingham, Alabama 35203</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="small-12 columns">
			<div class="copyright">
				Copyright <?php echo date('Y'); ?>. JEFFERSON COUNTY DISTRICT ATTORNEY. All rights reserved.
			</div>
		</div>
	</div>
</footer>

<script src="bower_components/jquery/jquery.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="js/jquery.equalheights.min.js" type="text/javascript"></script>
<script src="js/app.js"></script>
</body>
</html>