<?php
include($_SERVER['DOCUMENT_ROOT']."/tyfoon/connect.php");
//	$aOutput = pageGet( '73' );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = '';
	$layout = 'subpage';

	$wanted = photosAlbum('16');
?>

<?php
	include("header.php");
?>


<div class="row">
	<div class="large-12 columns">
		<div class="main-content">
			<div class="row">
				<div class="large-12 columns">
					<h2>Most Wanted Worthless Check Writers</h2>
					<div class="divider"></div>
					<? /* =$aOutput['msg']; */?>
					
					<div class="row">
						<div class="large-12 columns defendants">
							<?php 
						 	$iter = 0;

						 	foreach($wanted['images'] as $photo) { ?>
						 		<div class="defendant">
						 			<img src="<?=$photo['image'] ?>" alt="" />
						 			<p><?=$photo['title'] ?></a>

						 		</div>

						 	<?php 
						 	$iter++;
						 	if($iter == 4) {
						 		$iter = 0;
						 		echo '<div style="clear:both;"></div>';
						 	}

						 	} ?>
						</div>
					</div>

					<div class="row">
						<div class="small-8 end columns small-centered most-wanted-statement">
							<center><p>All suspects are presumed innocent until proven guilty in a court of law.</p>
							
							<p>Every effort is made to ensure the accuracy of information posted on this web site. However, the person(s) depicted may have been arrested within the past 72 hours and some names may be similar or identical to those of other individuals. For the most up to date and accurate information, contact the Jefferson County District Attorney’s Office Special Services Division.</p></center>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="divider"></div>

		<?php
			include("connected.php");
		?>
	</div>
</div>

<?php
	include("footer.php");
?>