<?php
	include($_SERVER ['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	include 'postman/_variables.php';
	$aNews = pageByCategory("NEWS","ALL", 0, 5, "PUBL_ASC");
	$aUpdates = pageByCategory("UPDATES", "ALL", 0, 5, "PUBL_ASC");
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>


<div class="row">
	<div class="large-12 columns">
		<div class="main-content hide-for-small">
			<div class="row">
				<div class="medium-4 columns">
					<img src="/img/brandon.jpg" alt="Brandon">
				</div>

				<div class="medium-8 columns">
					<h3>Brandon K. Falls, Jefferson County District Attorney</h3>

					<div class="divider"></div>

					<p>The District Attorney’s Office for the Tenth Judicial Circuit of Alabama is located in Jefferson County, Alabama. With a population of more than 659,000 people including 27 Police Departments covering 1,124 square miles, Jefferson County is the largest jurisdiction in Alabama. Our office is dedicated to protecting the rights and interests of all victims of crime while aggressively prosecuting those who engage in criminal activity. Utilizing the highest levels of professionalism, honor, integrity, and ethics in the execution of our responsibilities, we strive each day to earn the trust and respect of the citizens we are privileged to serve.</p>

					<p>This site was created as a resource guide for the citizens of Jefferson County to provide you with the information necessary to understand how cases are prosecuted through the criminal justice system. If you need assistance or additional information please see the <a href="contact-us.php">Contact Us</a> section.</p>
				</div>
			</div>
		</div>
		<div class="main-content show-for-small-only">
			<div class="row">
				<div class="small-4 columns">
					<img src="/img/brandon.jpg" alt="Brandon">
				</div>

				<div class="small-8 columns">
					<h3>Brandon K. Falls, Jefferson County District Attorney</h3>
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<div class="divider"></div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 columns">
					<p>The Tenth Judicial Circuit District Attorney’s Office is located in Jefferson County, Alabama. With a population of more than 658,000 and 27 Police Departments, Jefferson County is the largest jurisdiction in Alabama.</p>

					<p>This site was created as a resource guide for the citizens of Jefferson County to provide you with the information necessary to understand how cases are prosecuted through the criminal justice system. If you need assistance or additional information please see the contact us section.</p>
				</div>
			</div>
		</div>
		
		<?php
			include("connected.php");
		?>

		<div class="row updates">
			<div class="medium-6 small-12 columns">
				<h4>Important Updates</h4>
				<ul class="no-bullet">
					<?php foreach( $aUpdates as $aUpdate) {?>
					<li><span class="date"><? /*=date ( 'm/j/Y', strtotime($aUpdate['published']) ) ; */ ?></span><br>
						<a class="news-title" href="<?php echo $aUpdate['url']; ?>"><?php echo $aUpdate['title']; ?></a><br>
					</li>
					<?php } ?>
				</ul>
			</div>
			<div class="medium-6 small-12 columns">
				<h4>News Releases</h4>
				<ul class="no-bullet">
					<?php foreach( $aNews as $aArticle) {?>
					<li><span class="date"><? /*=date ( 'm/j/Y', strtotime($aUpdate['published']) ) ; */ ?></span><br>
						<a class="news-title" href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a><br>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php
	include("footer.php");
?>